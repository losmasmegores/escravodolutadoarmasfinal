using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectorMachine : MoveForward
{
    private enum SwitchMachineStates { NONE, SELECTINGAtack, SELECTINGEnemie, Waiting };
    //[SerializeField]
    private SwitchMachineStates m_CurrentState;
    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState) return;
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            //case SwitchMachineStates.SELECTINGAtack:
            //    m_SelectAttackAction.Enable();
            //    m_ChooseAttackAction.Enable();
            //    AttackGui.gameObject.SetActive(true);
            //    break;

            case SwitchMachineStates.SELECTINGEnemie:
                m_SelectSlaveAction.Enable();
                m_ChooseSlaveAction.Enable();
                mio.gameObject.SetActive(true);
                enemigo.gameObject.SetActive(true);
                global.intensity = 0.5f;
                break;


            case SwitchMachineStates.Waiting:
                break;


            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            //case SwitchMachineStates.SELECTINGAtack:
            //    m_SelectAttackAction.Disable();
            //    m_ChooseAttackAction.Disable();
            //    AttackGui.gameObject.SetActive(false);
            //    break;

            case SwitchMachineStates.SELECTINGEnemie:
                m_SelectSlaveAction.Disable();
                m_ChooseSlaveAction.Disable();
                mio.gameObject.SetActive(false);
                enemigo.gameObject.SetActive(false);
                global.intensity = 1.5f;
                break;

            case SwitchMachineStates.Waiting:

                break;

            default:
                break;
        }
    }
    //private void UpdateState()
    //{
    //    switch (m_CurrentState)
    //    {
    //        case SwitchMachineStates.SELECTINGAtack:


    //            break;

    //        case SwitchMachineStates.SELECTINGEnemie:


    //            break;

    //        case SwitchMachineStates.Waiting:

    //            break;

    //        default:
    //            break;
    //    }
    //}

    //------------------------------------------------------------------------//

    //public void EndHit()
    //{
    //    ChangeState(SwitchMachineStates.Waiting);
    //}
    //------------------------------------------------------------------------//

    //------------------------------------------------------------------------//
    //private void AttackAction(InputAction.CallbackContext actionContext)
    //{
    //    switch (m_CurrentState)
    //    {
    //        case SwitchMachineStates.SELECTINGAtack:

    //            break;

    //        case SwitchMachineStates.SELECTINGEnemie:

    //            break;

    //        case SwitchMachineStates.Waiting:

    //            break;

    //        default:
    //            break;
    //    }
    //}
    //------------------------------------------------------------------------//

    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputActionMap m_CurrentActionMap;
    private InputAction m_SelectAttackAction;
    private InputAction m_ChooseAttackAction;
    private InputAction m_SelectSlaveAction;
    private InputAction m_ChooseSlaveAction;
    private Rigidbody2D m_Rigidbody;

    [SerializeField]
    private GameEvent_SL_Transform m_GameEvent;

    [SerializeField]
    private GameEventPlayer m_PlayerEvent;

    //[SerializeField]
    //private Transform AttackGui;
    [SerializeField]
    private Transform SlavesEnemies;
    Transform actualSlave;

    [SerializeField]
    private Player nplayer;

    public String GetTplayer()
    {
        return nplayer+"";
    }

    private int countAttack;
    private int countEnemie;

    [SerializeField]
    private Light2D global;
    [SerializeField]
    private Light2D mio;
    [SerializeField]
    private Light2D enemigo;

    private int MaxCount;

    [SerializeField]
    private Slider slider;
    void Awake()
    {
        
        Assert.IsNotNull(m_InputAsset);
       
        m_Input = Instantiate(m_InputAsset);
        m_Input.bindingMask = InputBinding.MaskByGroup(this.GetTplayer());
        m_CurrentActionMap = m_Input.FindActionMap("BattleGui");
        //m_ChooseAttackAction = m_Input.FindActionMap("BattleGui").FindAction("ChooseAttack");
        //m_SelectAttackAction = m_Input.FindActionMap("BattleGui").FindAction("SelectAttack");
        m_ChooseSlaveAction = m_CurrentActionMap.FindAction("ChooseSlave");
        m_SelectSlaveAction = m_CurrentActionMap.FindAction("SelectSlave");
        //m_ChooseAttackAction.performed += MoveBteenAttacks;
        //m_SelectAttackAction.performed += SelectAttack;
        m_ChooseSlaveAction.performed += MoveBteenSlaves;
        m_SelectSlaveAction.performed += SelectSlave;
        countAttack = 0;
        countEnemie = 0;
        MaxCount = 3;
        //m_Input.FindActionMap("BattleGui").Enable(); 

    }

    private void Start()
    {
        ChangeState(SwitchMachineStates.Waiting);
    }

    void Update()
    {
        //UpdateState();
    }
    private void OnDisable()
    {

    }
    //private void MoveBteenAttacks(InputAction.CallbackContext actionContext)
    //{
    //    int maxcount = AttackGui.childCount;
    //    //Debug.Log(maxcount);
    //    countAttack = ChooseAction(actionContext, countAttack, maxcount);
    //    //Debug.Log(countAttack);
    //    for (int i = 0; i < maxcount; i++)
    //    {
    //        if (i == countAttack) AttackGui.GetChild(i).GetComponent<SpriteRenderer>().color = new Color(1,1,1,0.5f);
    //        else AttackGui.GetChild(i).GetComponent<SpriteRenderer>().color = new Color(0,0,0,0.5f);
    //    }


    //}
    //private void SelectAttack(InputAction.CallbackContext actionContext)
    //{
    //    Debug.Log(countAttack + " es el ataque");
    //    ChangeState(SwitchMachineStates.SELECTINGEnemie);
    //}
    private void MoveBteenSlaves(InputAction.CallbackContext actionContext)
    {

        countEnemie = ChooseAction(actionContext, countEnemie, MaxCount);
        //Debug.Log(countEnemie);



        for (int i = 0; i < MaxCount; i++)
        {
            if (i == countEnemie)
            {
                if (SlavesEnemies.GetChild(i).gameObject.activeSelf)
                {
                    enemigo.transform.position = SlavesEnemies.GetChild(i).transform.position;
                }

                //slider.transform.position = SlavesEnemies.GetChild(i).transform.position;
                //slider.value = (int)SlavesEnemies.GetChild(i).GetComponent<esclavito>().Vida;
                //SlavesEnemies.GetChild(i).GetComponent<SpriteRenderer>().color = Color.blue;
            }
            //else SlavesEnemies.GetChild(i).GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

    private void SelectSlave(InputAction.CallbackContext actionContext)
    {
        //for (int i = 0; i < SlavesEnemies.childCount; i++)
        //{
        //    SlavesEnemies.GetChild(i).GetComponent<SpriteRenderer>().color = Color.white;
        //}
        //Debug.Log(SlavesEnemies.GetChild(countEnemie).GetComponent<esclavito>().GetEsclavo);
        ChangeState(SwitchMachineStates.Waiting);
        m_GameEvent.Raise(actualSlave, SlavesEnemies.GetChild(countEnemie).transform);
    }


    private void OnDestroy()
    {
        //m_ChooseAttackAction.performed -= MoveBteenAttacks;
        //m_SelectAttackAction.performed -= SelectAttack;
        m_ChooseSlaveAction.performed -= MoveBteenSlaves;
        m_SelectSlaveAction.performed -= SelectSlave;
    }


    public void teToca(ScriptableSlave sclavito)
    {
     
        for (int i = 0; i < transform.childCount; i++)
        {
               
                if (sclavito == transform.GetChild(i).GetComponent<esclavito>().GetEsclavo)
                {
                    actualSlave = transform.GetChild(i).transform;
                    //actualSlave.GetComponent<SpriteRenderer>().color = Color.magenta;
                    mio.transform.position = actualSlave.transform.position;

                    Debug.Log(this.GetTplayer());
                    m_PlayerEvent.Raise(nplayer);

                    ChangeState(SwitchMachineStates.SELECTINGEnemie);
                    return;
                }
        }
               
                //else { transform.GetChild(i).GetComponent<SpriteRenderer>().color = Color.white; }
    }
    
        
      


        //Debug.Log("te toca " + tplayer);
        //string player = "";
        //if (tplayer == Player.Player1 + "") { player = "Player1"; }
        //else if (nplayer == Player.Player2) { player = "Player2"; }
        //m_CurrentActionMap = m_Input.FindActionMap("BattleGui");
        //m_CurrentActionMap.bindingMask = InputBinding.MaskByGroup(player);
        //ChangeState(SwitchMachineStates.SELECTINGEnemie);
}



