using m08m17;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering.Universal;

public class Selector : MoveForward
{

    public GameEvent evento;

    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputActionMap m_CurrentActionMap;
    private InputAction m_ChooseAction;
    private InputAction m_SelectAction;

    [SerializeField]
    private Player nplayer;
    private int count;
    private int maxcount;

    [SerializeField]
    private ScriptableSelector warriors;
    private int actualslave;
    private bool canselect;

    //[SerializeField]
    //private Light2D light;

    void Awake()
    {
        //String player = "\""+nplayer+"\"";
        //Debug.Log(player);
        String player = "";
        if(nplayer == Player.Player1) { player = "Player1"; }
        else if(nplayer == Player.Player2) { player = "Player2"; }

        actualslave = 0;

        m_Input = Instantiate(m_InputAsset);
        m_CurrentActionMap = m_Input.FindActionMap("Player");
        m_CurrentActionMap.bindingMask = InputBinding.MaskByGroup(player);
        m_ChooseAction = m_CurrentActionMap.FindAction("Choose");
        m_SelectAction = m_CurrentActionMap.FindAction("Select");
        m_ChooseAction.performed += SelectRow;
        m_SelectAction.performed += SelectAction;
        m_CurrentActionMap.Enable();
        count = 0;
        maxcount = transform.childCount;
        canselect = true;
        //SelectRow();


        for (int i = 0; i < maxcount; i++)
        {
            //if (i == count)
            //    transform.GetChild(i).gameObject.SetActive(true);
            //else
            //    transform.GetChild(i).gameObject.SetActive(false);
            if (i == count)
            {
                transform.GetChild(i).gameObject.SetActive(true);
                //light.transform.position = new Vector3(transform.GetChild(i).position.x, light.transform.position.y, 0);
            }
            else
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //private void ChooseAction(InputAction.CallbackContext actionContext)
    //{
    //   if(m_ChooseAction.ReadValue<float>() > 0 && count < maxcount-1)
    //    {
    //        count++;
    //    }
    //    else if(m_ChooseAction.ReadValue<float>() < 0 && count > 0)
    //    {
    //        count--;
    //    }
    //    SelectRow();
    //}
    //Choose
    private void SelectRow(InputAction.CallbackContext actionContext)
    {
        count = ChooseAction(actionContext,count,maxcount);
        for (int i = 0; i < maxcount; i++)
        {
            if (i == count)
            {
                transform.GetChild(i).gameObject.SetActive(true);
                //light.transform.position = new Vector3(transform.GetChild(i).position.x, light.transform.position.y, 0);
                Debug.Log("lo hice");
            }
            else
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    private void SelectAction(InputAction.CallbackContext actionContext)
    {
        if (actualslave < 3 && canselect)
        {
            transform.GetChild(count).GetChild(getAvaiblechilds(transform.GetChild(count))).gameObject.SetActive(false);
            warriors.Warriors[actualslave].MiClasse = transform.GetChild(count).GetComponent<Selectable>().getClass();
            actualslave++;
            if (actualslave == 3)
            {
                canselect = false;
                ActivarEvento();
            }
        }
    }
    private int getAvaiblechilds(Transform parent)
    {
        int num = -1;
        for( int i = 0;i < parent.childCount; i++)
        {
            if (parent.GetChild(i).gameObject.activeSelf) num++;
        }
        return num;
    }
    public void ActivarEvento()
    {
        Debug.Log("Activando");
        evento.Raise();
    }
    private void OnDestroy()
    {
        m_ChooseAction.performed -= SelectRow;
        m_SelectAction.performed -= SelectAction;
        m_CurrentActionMap.Disable();
    }
}
