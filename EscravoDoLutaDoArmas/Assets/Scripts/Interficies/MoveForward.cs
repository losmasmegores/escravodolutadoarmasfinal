using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.RuleTile.TilingRuleOutput;

public class MoveForward : MonoBehaviour
{
    public int ChooseAction(InputAction.CallbackContext actionContext, int count, int maxcount)
    {
        count = (count + (int)actionContext.ReadValue<float>()) % maxcount;
        return count < 0? maxcount + count: count;

        if (actionContext.ReadValue<float>() > 0) //
        {
            //count = (count + 1) % maxcount;
            count++;
        }
        else if (actionContext.ReadValue<float>() < 0) // 
        {
            
            //count--;
        }
        return count;
    }
}
