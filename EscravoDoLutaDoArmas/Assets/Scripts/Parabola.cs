using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.PackageManager.UI;
using UnityEditor.ShaderGraph.Internal;
using UnityEngine;
using UnityEngine.UIElements;

public class Parabola : MonoBehaviour
{
    [SerializeField]
    public GameObject target;
    [SerializeField]
    public GameObject torre;
    private float duracion = 3f;

    void Start()
    {

        // Calcular la velocidad en la direcci�n x
        float v0_x = (target.transform.position.x - torre.transform.position.x) / (duracion);

        // Aislar v0 en la segunda ecuaci�n
        float v0_y = ((target.transform.position.y - torre.transform.position.y) + (0.5f * 9.8f * duracion * duracion)) / (duracion);

        // Configurar el Rigidbody2D
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(v0_x, v0_y);
        transform.position = torre.transform.position;
    }

}
