using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class esclavito : MonoBehaviour, ISaveableObject
{
    [SerializeField]
    private ScriptableSlave esclavadito;
    public ScriptableSlave GetEsclavo => esclavadito;

    
    public string uniqueID;

    private double speed;
    [SerializeField]
    private double dmg;
    [SerializeField]
    private double vida;
    private double vidamax;
    private bool casco;
    private bool arma;
    private bool botas;
    private string animation;
    private float ariaRange;
    private HitboxInfo hitboxInfo;

    public poolManager pool;


    void Awake()
    {
        esclavadito.Crear();
        this.speed = esclavadito.GetSpeed;
        this.dmg = esclavadito.GetDmg;
        this.vida = esclavadito.GetVida;
        this.vidamax = esclavadito.GetVida;
        this.casco = esclavadito.Casco;
        this.arma = esclavadito.Arma;
        this.botas = esclavadito.Botas;
        esclavadito.trans = this.transform.position;
        animation = getClasseAnim();
        ariaRange = getClasseArea();
        //hitboxInfo = transform.GetComponent<HitboxInfo>();
        //if (hitboxInfo) ;
            //hitboxInfo.OnResta += morir;
    }


    public double Vida
    {
        get { return vida; }
        set { vida = value; }
    }
    public double VidaMax => vidamax;
    public double Damage
    {
        get { return dmg; }
        set { dmg = value; }
    }

    public void Danar(double dano)
    {
        this.Vida -= dano;
        if (this.Vida <= 0)
        {
            this.MeMuero();
        }
        //Debug.Log(this.name + "Tengo " + this.Vida);
    }
    public string anim
    {
        get { return animation; }
    }
    public float area
    {
        get { return ariaRange; }
    }
    private string getClasseAnim()
    {
        switch (esclavadito.MiClasse){
            case Classes.Warrior:
                return "attack";
            case Classes.Archer:
                return "shoot";
            case Classes.Magician:
                return "spell";
            default:
                return null;
        }
    }
    private float getClasseArea()
    {
        switch (esclavadito.MiClasse)
        {
            case Classes.Warrior:
                return 1;
            case Classes.Archer:
                return 7;
            case Classes.Magician:
                return 7;
            default:
                return 0;
        }
    }

    private void MeMuero()
    {
        this.gameObject.SetActive(false);
    }

    public void Load(SaveData.SlaveData _slaveData)
    {
        this.speed = _slaveData.speed;
        this.dmg = _slaveData.dmg;
        this.vida = _slaveData.vida;
        this.casco = _slaveData.casco;
        this.arma = _slaveData.arma;
        this.botas = _slaveData.botas;

        UpdateEsclavitoProperties();
    }

    private void UpdateEsclavitoProperties()
{
    esclavadito.Speed = this.speed;
    esclavadito.Damage = this.dmg;
    esclavadito.Vida = this.vida;
    esclavadito.Casco = this.casco;
    esclavadito.Arma = this.arma;
    esclavadito.Botas = this.botas;
}

    public SaveData.SlaveData Save()
    {
        return new SaveData.SlaveData(this.speed, this.dmg, this.vida, this.casco, this.arma, this.botas, this.uniqueID);
    }

    public void Init()
    {
        this.speed = 1;
        this.dmg = 1;
        this.vida = 1;
        this.casco = false;
        this.arma = false;
        this.botas = false;
    }

}
