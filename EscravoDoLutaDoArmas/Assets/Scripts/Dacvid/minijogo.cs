using System.Collections;
using System.Collections.Generic;
using UnityEditor.Timeline.Actions;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

public class minijogo : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private Rigidbody2D m_RigidBody;
    [SerializeField]
    private float speed;

    void Awake()
    {
        m_Input = Instantiate(m_InputAsset);
        m_Input.FindActionMap("jugar").FindAction("pj1").started += restar;
        m_Input.FindActionMap("jugar").FindAction("pj2").started += sumar;
        m_Input.FindActionMap("jugar").Enable();
        m_RigidBody = this.GetComponent<Rigidbody2D>();

    }

    public void restar(InputAction.CallbackContext icc)
    {
        this.m_RigidBody.AddForce(new Vector2(-speed, 0), ForceMode2D.Impulse);
        //this.transform.position = new Vector2(this.transform.position.x-1,0);
    }
    public void sumar(InputAction.CallbackContext icc)
    {
        this.m_RigidBody.AddForce(new Vector2(speed, 0), ForceMode2D.Impulse);
        //this.transform.position = new Vector2(this.transform.position.x+1,0);
    }
    private void OnDestroy()
    {
        m_Input.FindActionMap("jugar").FindAction("pj1").started -= restar;
        m_Input.FindActionMap("jugar").FindAction("pj2").started -= sumar;
        m_Input.FindActionMap("jugar").Disable();
    }

}
