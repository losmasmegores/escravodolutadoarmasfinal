using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public void ChangeLoadedGame()
    {
        SceneManager.LoadScene("Game");
    }
    public void ChangeEscollir()
    {
        SceneManager.LoadScene("Seleccion");
    }
    public void ChangeExit()
    {
        SceneManager.LoadScene("Exit");
    }
}
