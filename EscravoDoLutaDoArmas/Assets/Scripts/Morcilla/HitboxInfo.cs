using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxInfo : MonoBehaviour
{
    public double dmg;

    private void Start()
    {
        if(this.GetComponentInParent<esclavito>())
            dmg = this.GetComponentInParent<esclavito>().Damage;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Colisiono con: " + collision.name);
        collision.GetComponent<esclavito>().Danar(dmg);
        if (this.transform.CompareTag("bala"))
        {
           this.gameObject.SetActive(false);
        }
    }
}
