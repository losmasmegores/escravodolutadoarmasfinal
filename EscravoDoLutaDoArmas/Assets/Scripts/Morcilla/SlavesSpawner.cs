using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlavesSpawner : MonoBehaviour
{
    [SerializeField]
    private esclavito[] slavesInScene;
    public void FromSaveData(SaveData data)
    {
        Debug.Log("He entrado en FromSaveData");

        foreach (SaveData.SlaveData slaveData in data.m_Slaves)
        {
            esclavito slaveToLoad = FindSlaveByID(slaveData.uniqueID);
            if (slaveToLoad != null)
            {
                slaveToLoad.Load(slaveData);
                slaveToLoad.Init();
            }
        }
    }

    private esclavito FindSlaveByID(string uniqueID)
    {
        foreach (esclavito slave in slavesInScene)
        {
            if (slave.uniqueID == uniqueID)
            {
                return slave;
            }
        }
        return null;
    }
}

