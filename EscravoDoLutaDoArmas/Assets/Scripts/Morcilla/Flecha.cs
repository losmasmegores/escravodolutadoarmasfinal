using UnityEngine;

public class Flecha : MonoBehaviour
{
    public float velocidadInicial = 10f;
    public float anguloLanzamiento = 45f;
    private float gravedad = 9.81f;

    private float tiempoVuelo;
    private float tiempoActual = 0f;

    private void Start()
    {
        // Calcular el tiempo de vuelo
        tiempoVuelo = (2 * velocidadInicial * Mathf.Sin(anguloLanzamiento * Mathf.Deg2Rad)) / gravedad;
    }

    private void Update()
    {
        if (tiempoActual < tiempoVuelo)
        {
            // Calcular la posici�n en x e y
            float x = tiempoActual * velocidadInicial * Mathf.Cos(anguloLanzamiento * Mathf.Deg2Rad);
            float y = tiempoActual * velocidadInicial * Mathf.Sin(anguloLanzamiento * Mathf.Deg2Rad) -
                      0.5f * gravedad * tiempoActual * tiempoActual;

            // Actualizar la posici�n de la flecha
            transform.position = new Vector3(x, y, 0);

            tiempoActual += Time.deltaTime;
        }
        else
        {
            // La flecha ha alcanzado el suelo o su destino, puedo desactivarla o realizar otras acciones.
            gameObject.SetActive(false);
        }
    }
}
