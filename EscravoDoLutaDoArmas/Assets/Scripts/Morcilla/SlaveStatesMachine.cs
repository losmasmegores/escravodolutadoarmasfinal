using m08m17;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using Unity.VisualScripting.Antlr3.Runtime.Tree;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class SlaveStatesMachine : MonoBehaviour
{
    private enum EnemyStates { IDLE, WALK, RETURN, ATTACK1, SPECIALATTACK }
    //private enum Enemies { RANGED, MELEE } //ENUM DISPONIBLE --> Classe
    //private Enemies enemy;
    [SerializeField]
    private EnemyStates currentState;

    private Transform player; // Referencia al jugador
    [SerializeField]
    private float attackRange;
    private Rigidbody2D m_Rigidbody;
    private Animator m_Animator;
    Vector3 initialPosition;
    [SerializeField]
    private float moveSpeed = 1f;

    private Transform enemigo;

    [SerializeField]
    private GameEvent evento;

    private string animation;
    


    /*private List<GameObject> objectPool = new List<GameObject>();
    public int poolSize = 3;
    public GameObject projectilePrefab;
    public float fireRate = 2.0f;*/
    //private float nextFireTime = 0.0f;
    /*public float bulletSpeed = 10f;*/

    private bool seleccionado = false;

    [SerializeField] private LayerMask mPlayerLayer;



    // Otros atributos y variables necesarios
    void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        initialPosition = transform.position;
        animation = GetComponent<esclavito>().anim;
        attackRange = GetComponent<esclavito>().area;
        /* if (this.enemy == Enemies.RANGED)
         {  
             InitializeBulletPool();
         }*/

    }

    /*void InitializeBulletPool()
    {
        for (int i = 0; i < poolSize; i++)
        {
            GameObject bullet = Instantiate(projectilePrefab, transform);
            bullet.SetActive(false);
            objectPool.Add(bullet);
        }
    }*/

    void Start()
    {
        //initialPosition = transform.position;
        /*if (this.enemy == Enemies.RANGED)
        {
            ChangeState(EnemyStates.PATRULLAR);
        }
        else
        {
            ChangeState(EnemyStates.IDLE); // Inicia en estado "idle"
        }*/
        ChangeState(EnemyStates.IDLE); // Inicia en estado "idle"

    }

    void Update()
    {
        
        UpdateState();
       
    }

    void ChangeState(EnemyStates newState)
    {
        if (newState == currentState)
            return;

        ExitState();
        InitState(newState);
    }

    void InitState(EnemyStates newState)
    {
        currentState = newState;
        switch (currentState)
        {
            case EnemyStates.IDLE:

                m_Rigidbody.velocity = Vector2.zero;
                //m_Animator.Play("idle");

                break;

            case EnemyStates.WALK:
                MoverEsclavo(enemigo.position);
                //m_Animator.Play("caminar");

                break;
            case EnemyStates.RETURN:
                MoverEsclavo(initialPosition);
                break;

            case EnemyStates.ATTACK1:
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play(animation);
                break;

            case EnemyStates.SPECIALATTACK:
                break;
        }
    }

    void ExitState()
    {
        switch (currentState)
        {
            case EnemyStates.IDLE:

                break;

            case EnemyStates.WALK:

                break;
            case EnemyStates.RETURN:

                break;

            case EnemyStates.ATTACK1:
                break;

            case EnemyStates.SPECIALATTACK:
                break;

            default:
                break;
        }
    }
    public void EndHit()
    {
        /*if (this.enemy == Enemies.RANGED)
        {
            ChangeState(EnemyStates.PATRULLAR);
        }
        else
        {
            ChangeState(EnemyStates.IDLE);
        }*/
        ChangeState(EnemyStates.RETURN);

    }

    void UpdateState()
    {
        switch (currentState)
        {
            case EnemyStates.IDLE:
                break;

            case EnemyStates.WALK:

                if (PlayerRange(attackRange))
                {
                    //Debug.Log("paro");
                    ChangeState(EnemyStates.ATTACK1);
                    //ChangeState(EnemyStates.RETURN);
                }
                break;

            case EnemyStates.RETURN:
                if (Vector3.Distance(transform.position,initialPosition) < 0.2f)
                {
                    m_Rigidbody.MovePosition(initialPosition);
                    ChangeState(EnemyStates.IDLE);
                    evento.Raise();
                    //MoverEsclavo(initialPosition);
                }
                break;

            case EnemyStates.ATTACK1:

                //if (this.enemy == Enemies.RANGED)
                //{
                //    if (Time.time > nextFireTime)
                //    {
                //        Fire();
                //        nextFireTime = Time.time + fireRate;
                //    }
                //}
                break;

            case EnemyStates.SPECIALATTACK:
                break;

            default:
                break;
        }
    }



    bool PlayerRange(float distance)
    {
        // Lgica para verificar si el jugador est en el rango de ataque
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, distance, Vector2.up, distance, mPlayerLayer);
        if (hit.rigidbody != null)
        {
            player = hit.rigidbody.transform;
            return true;
        }
        return false;
    }

    public void MoverEsclavo(Vector3 t)
    {
        // Lgica para perseguir al jugador
        Vector3 playerPosition = t;
        Vector3 enemyPosition = transform.position;

        // Calcula la direccin hacia el jugador
        Vector3 moveDirection = (playerPosition - enemyPosition).normalized;


        // Mueve al enemigo hacia el jugador
        m_Rigidbody.velocity = (moveDirection * moveSpeed);
        if (m_Rigidbody.velocity.x > 0)
        {
            this.transform.eulerAngles = Vector3.zero;
        }
        else if (m_Rigidbody.velocity.x < 0)
        {
            this.transform.eulerAngles = Vector3.up * 180;
        }
    }

    private Vector2 apunta;

    void Fire()
    {

        bullet = this.GetComponent<esclavito>().pool.BalaRequest();
        bullet.GetComponent<HitboxInfo>().dmg = this.GetComponent<esclavito>().Damage;
        if (bullet == null)
        {
            return;
        }

        apunta = enemigo.position - this.transform.position;
        this.transform.up = apunta;


        bullet.transform.position = this.transform.position;

        Vector3 direction = (enemigo.position - bullet.transform.position).normalized;

        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            rb.velocity = direction * 9;
        }

    }

    private GameObject bullet;

    void FireParabola()
    {
        bullet = this.GetComponent<esclavito>().pool.BalaRequest();
        bullet.GetComponent<Rigidbody2D>().gravityScale = 1;
        bullet.GetComponent<HitboxInfo>().dmg = this.GetComponent<esclavito>().Damage;
        if (bullet == null)
        {
            return;
        }

        apunta = enemigo.position - this.transform.position;
        this.transform.up = apunta;

        float v0_x = (enemigo.position.x - this.transform.position.x) / (3);
        float v0_y = ((enemigo.position.y - this.transform.position.y) + (0.5f * 9.8f * 3 * 3)) / (3);

        bullet.transform.position = this.transform.position;
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(v0_x, v0_y);


    }



    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, attackRange);
    }


    /*public void Init(EnemyVariant e)
    {
        GetComponentInChildren<HitboxInfo>(true).Damage = e.damage;
        GetComponent<SpriteRenderer>().color = e.color;
        moveSpeed = e.speed;
    }*/
    public void metocaAtacar(Transform yo, Transform enemie)
    {
        if(yo == this.transform)
        {
            //Debug.Log("Soy yo ");
            enemigo = enemie;
            ChangeState(EnemyStates.WALK);
        }
        //else Debug.Log("No soy yo");
    }


}

