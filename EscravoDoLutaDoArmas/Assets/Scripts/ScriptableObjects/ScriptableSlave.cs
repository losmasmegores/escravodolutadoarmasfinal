using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.Collections.LowLevel.Unsafe;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableSlave", menuName = "Scriptable Objects/Scriptable Slave")]
[Serializable]
public class ScriptableSlave: ScriptableObject, IComparable<ScriptableSlave>
{
    [SerializeField]
    private Classes m_Class;

    [SerializeField]
    private double m_Speed;
    [SerializeField]
    private double m_Damage;
    [SerializeField]
    private double m_Vida;
    [SerializeField]
    private bool casco;
    [SerializeField]
    private bool arma;
    [SerializeField]
    private bool botas;
    [SerializeField]
    public Vector3 trans;

    public Classes MiClasse
    {
        get { return m_Class; }
        set { m_Class = value; }
    }
    public bool Casco
    {
        get { return casco; }
        set { casco = value; }
    }
    public bool Arma
    {
        get { return arma; }
        set { arma = value; }
    }
    public bool Botas
    {
        get { return botas; }
        set { botas = value; }
    }

    public double Speed
    {
        get { return m_Speed; }
        set { m_Speed = value; }
    }

    public double Damage
    {
        get { return m_Damage; }
        set { m_Damage = value; }
    }

    public double Vida
    {
        get { return m_Vida; }
        set { m_Vida = value; }
    }

    public double GetSpeed => m_Speed;
    public double GetDmg => m_Damage;
    public double GetVida => m_Vida;
    public int CompareTo(ScriptableSlave other)
    {
        if (this.m_Speed == other.m_Speed) return 0;
        if (this.m_Speed < other.m_Speed) return -1;
        if (this.m_Speed > other.m_Speed) return 1;

        return 0;
    }

    public void Crear()
    {
        switch (m_Class)
        {
            case Classes.Warrior:
                this.m_Speed = 2;
                this.m_Damage = 1;
                this.m_Vida = 12;
            break;
            
            case Classes.Archer:
                this.m_Speed = 4;
                this.m_Damage = 2.5;
                this.m_Vida = 8;
                break;
            case Classes.Magician:
                this.m_Speed = 2.5;
                this.m_Damage = 1.5;
                this.m_Vida = 10;
                break;
        }

        if (this.casco)
        {
            this.m_Vida += 3;
        }
        if (this.arma)
        {
            this.m_Damage += 2;
        }
        if(this.botas){
            this.m_Speed += 2;
        }
    }

   

}
