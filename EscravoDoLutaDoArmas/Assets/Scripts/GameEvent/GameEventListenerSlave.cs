using UnityEngine;
using UnityEngine.Events;

public class GameEventListenerSlave : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEventSlave Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<ScriptableSlave> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(ScriptableSlave a)
    {
        Response.Invoke(a);
    }
}
