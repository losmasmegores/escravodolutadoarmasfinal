using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListenerPlayer : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEventPlayer Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<Player> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(Player a)
    {
        Response.Invoke(a);
    }
}
