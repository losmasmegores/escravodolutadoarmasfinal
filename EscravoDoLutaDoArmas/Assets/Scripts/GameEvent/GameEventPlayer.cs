using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEventPlayer")]
public class GameEventPlayer : ScriptableObject
{
    private readonly List<GameEventListenerPlayer> eventListeners =
        new List<GameEventListenerPlayer>();

    public void Raise(Player a)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(a);
    }

    public void RegisterListener(GameEventListenerPlayer listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerPlayer listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
