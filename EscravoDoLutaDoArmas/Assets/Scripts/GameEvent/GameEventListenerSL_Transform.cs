using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListenerSL_Transform :MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEvent_SL_Transform Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<Transform, Transform> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(Transform a, Transform b)
    {

        Response.Invoke(a, b);
    }
}
