using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEventTransform")]
public class GameEvent_SL_Transform : ScriptableObject
{
    private readonly List<GameEventListenerSL_Transform> eventListeners =
        new List<GameEventListenerSL_Transform>();

    public void Raise(Transform a, Transform b)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(a, b);
    }

    public void RegisterListener(GameEventListenerSL_Transform listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerSL_Transform listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
