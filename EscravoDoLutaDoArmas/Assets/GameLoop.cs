using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLoop : MonoBehaviour
{
    [SerializeField]
    private Transform inv1;
    [SerializeField]
    private Transform inv2;

    [SerializeField]
    private int contador1 = 0;
    [SerializeField]
    private int contador2 = 0;

    [SerializeField]
    private List<ScriptableSlave> orden;
    private int actualcount = -1;

    public GameEventSlave tetocaAti;

    void Start()
    {
        orden = new List<ScriptableSlave>();
        añadirDicc();
        pasaturno();
    }

    private void añadirDicc()
    {
        for (int i = 0; i < 3; i++)
        {
            //Debug.Log(inv1.GetChild(i).GetComponent<esclavito>().GetEsclavo);
            orden.Add(inv1.GetChild(i).GetComponent<esclavito>().GetEsclavo);
            orden.Add(inv2.GetChild(i).GetComponent<esclavito>().GetEsclavo);
        }
        orden.Sort();
        orden.Reverse();
    }

    public void pasaturno()
    {
        comprobarVIdas();
        if (contador1 >= 3 || contador2 >= 3)
        {

            SceneManager.LoadScene("Seleccion");
        }
        else
        {
            // Queda lo posicion sobrante de la suma de los dos entre el tamanyo de la lista. cuando sea igual el residuo sera 0
            actualcount = (actualcount + 1) % orden.Count;
            Debug.Log(orden[actualcount].name);
            tetocaAti.Raise(orden[actualcount]);
        }
    }

    public void comprobarVIdas()
    {
        for (int i = 0; i < inv1.childCount; i++)
        {
            if (!inv1.GetChild(i).gameObject.activeSelf)
            {
                contador1++;

            }


            if (!inv2.GetChild(i).gameObject.activeSelf)
            {
                contador2++;
            }
        }
    }
}

